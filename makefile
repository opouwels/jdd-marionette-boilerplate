all: build

clean:
	rm -rf dist/

prepare:
	cp -R src/static dist
#	mkdir -p dist/node_modules/font-awesome
#	cp -R node_modules/font-awesome public/dist

build: clean prepare
	browserify -o dist/app.js src/init.js

build-production: clean prepare
	browserify -g uglifyify -o dist/app.js src/init.js

serve:
	php -S localhost:8080 --docroot dist

watch:
	nodemon -w src -e js,less,css,hbs,html -x "make"

push:
	aws s3 sync --acl public-read --profile ${awsprofile}  --region eu-central-1 public/ s3://${awsbuckurl}/
