var $ = jQuery = require('jquery');
var app = require('./app');

window.$ = $;

window.app = new app({container: 'body'});
window.app.start();
